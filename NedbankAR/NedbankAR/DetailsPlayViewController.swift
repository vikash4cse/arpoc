//
//  DetailsPlayViewController.swift
//  NedbankAR
//
//  Created by Rupender on 04/07/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import UIKit
import AVFoundation
import MessageUI
import YoutubePlayer_in_WKWebView

class DetailsTableCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
}

class DetailsPlayViewController: UIViewController, WKYTPlayerViewDelegate, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var playerView: WKYTPlayerView!
    @IBOutlet weak var playerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    let playlist: [String] = ["V0LC32Uogtw","LwGpKCTM9w8","mBCckw42fo4","Co7_8uA4r-o","r69SuvFBAvo","yeDW7F-m1Ug","O5z8smMW-ew"]
    let titleArray: [String] = ["Coach Dingaan Episode 1: 'Man of The Match'","Coach Dingaan Episode 2: 'Captain my captain.'","Coach Dingaan Episode 3: 'Endorsement.'","Coach Dingaan: Episode 4 - Nedbank Funeral Insurance","Coach Dingaan: Episode 5 - Embrace the competition","Coach Dingaan: Episode 6 - Ke Yona Team vs Mamelodi Sundowns", "Coach Dingaan: Episode 7 - Ke Yona Challenge Results"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.playerHeightConstraint.constant = 0.0
        self.playerView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        //self.playVideo(from: "Nedbank.mp4")
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func infoTapped(_ sender: Any) {
        let emailTitle = "Nedbank videos"
        let messageBody = "Awsome videos from Nedbank"
        let toRecipents = ["rupender.rana@nagarro.com"]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        
        self.present(mc, animated: true, completion: nil)
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .cancelled :
            print("Mail cancelled")
        case .saved :
            print("Mail saved")
        case .sent :
            print("Mail sent")
        case .failed :
            print("Mail sent failure: \(error?.localizedDescription ?? "")")
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    private func playVideo(at: Int) {
        
        //playerView.load(withVideoId: playlist[at])
        playerView.load(withVideoId: playlist[at] , playerVars: ["playsinline" : 1])
    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        playerView.playVideo()
    }
}

extension DetailsPlayViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return playlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: DetailsTableCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailsTableCell
        cell.backgroundImageView.image = UIImage(named: "NedBank")
        cell.titleLabel.text = titleArray[indexPath.row]
        //cell.backgroundImageView.image =
        let videoID = playlist[indexPath.row]
        cell.backgroundImageView.downloadImageFrom(link: "https://img.youtube.com/vi/\(videoID)/mqdefault.jpg", contentMode: .redraw)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if playerHeightConstraint.constant == 0.0 {
            UIView.animate(withDuration: 0.8, delay: 0.1, options: [.repeat, .curveEaseOut, .autoreverse], animations: {
                
                self.playerHeightConstraint.constant = 200
                
            }, completion: { _ in
                self.playVideo(at: indexPath.row)
            })
        } else {
            self.playVideo(at: indexPath.row)
        }
    }
}

extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        
        URLSession.shared.dataTask(with: URL(string:link)!) { (data, response, error) in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }.resume()
    }
}
