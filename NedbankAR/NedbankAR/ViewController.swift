//
//  ViewController.swift
//  NedbankAR
//
//  Created by Rupender on 04/07/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import CoreMedia
// git clone https://rupender90@bitbucket.org/vikash4cse/arpoc.git

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

class ViewController: UIViewController, OverlayDelegate {
    
   @IBOutlet var sceneView: ARSCNView!
   private var imageConfiguration: ARImageTrackingConfiguration?
   let playOverlay: PlayOverlay = UIView.fromNib()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        self.playOverlay.handler = self
        self.addTapGestureToSceneView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.navigationController?.isNavigationBarHidden = true
        let scene = SCNScene()
        sceneView.scene = scene
        setupImageDetection()
        if let configuration = imageConfiguration {
            sceneView.session.run(configuration)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) in
            node.removeFromParentNode()
        }
    }
    
    // MARK: - Configuration functions to fill out
    
    private func setupImageDetection() {
        imageConfiguration = ARImageTrackingConfiguration()
        guard let referenceImages = ARReferenceImage.referenceImages(
            inGroupNamed: "AR Images", bundle: nil) else {
                fatalError("Missing expected asset catalog resources.")
        }
        imageConfiguration?.trackingImages = referenceImages
    }
    
    private func setupObjectDetection() {
    
    }
    
    func tapReceived() {
        DispatchQueue.main.async {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DetailsPlayViewController")
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    func addTapGestureToSceneView() {
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.didTap(withGestureRecognizer:)))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func didTap(withGestureRecognizer recognizer: UIGestureRecognizer) {
        
        let tapLocation = recognizer.location(in: sceneView)
        let hitTestResults = sceneView.hitTest(tapLocation)
        guard let node = hitTestResults.first?.node else { return }
        //node.removeFromParentNode()
        self.tapReceived()
    }
}

// MARK: -
extension ViewController: ARSessionDelegate {
    func session(_ session: ARSession, didFailWithError error: Error) {
        guard
            let error = error as? ARError,
            let code = ARError.Code(rawValue: error.errorCode)
            else { return }
        switch code {
        case .cameraUnauthorized:
            print("Camera tracking is not available. Please check your camera permissions.")
        default:
            print("Error starting ARKit. Please fix the app and relaunch.")
        }
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .limited(let reason):
            switch reason {
            case .excessiveMotion:
                print("Too much motion! Slow down.")
            case .initializing, .relocalizing:
                print("ARKit is doing it's thing. Move around slowly for a bit while it warms up.")
            case .insufficientFeatures:
                print("Not enough features detected, try moving around a bit more or turning on the lights.")
            }
        case .normal:
            print("Point the camera at Image.")
        case .notAvailable:
            print("Camera tracking is not available.")
        }
    }
}

// MARK: -
extension ViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        if let imageAnchor = anchor as? ARImageAnchor {
            handleFoundImage(imageAnchor, node)
        }
    }
    
    private func handleFoundImage(_ imageAnchor: ARImageAnchor, _ node: SCNNode) {
        let name = imageAnchor.referenceImage.name!
        print("you found a \(name) image")
        
        let size = imageAnchor.referenceImage.physicalSize
        DispatchQueue.main.async {
            if let videoNode = self.makeNedbankVideo(size: size) {
                node.addChildNode(videoNode)
                node.opacity = 0.9
            }
        }
    }
    
    private func makeNedbankVideo(size: CGSize) -> SCNNode? {
        
        let file = "Nedbank.mp4".components(separatedBy: ".")
        if let path = Bundle.main.path(forResource: file[0], ofType:file[1]) {
            debugPrint( "\(file.joined(separator: ".")) not found")
            let avPlayerItem = AVPlayerItem(url: URL(fileURLWithPath: path))
            let avPlayer = AVPlayer(playerItem: avPlayerItem)
            avPlayer.play()
            
            NotificationCenter.default.addObserver(
                forName: .AVPlayerItemDidPlayToEndTime,
                object: nil,
                queue: nil) { notification in
//                    avPlayer.seek(to: .zero)
//                    avPlayer.play()
                    self.tapReceived()
            }
            
            let playMaterial = SCNMaterial()
            playMaterial.diffuse.contents = avPlayer
            
            let videoPlane = SCNPlane(width: size.width, height: size.height)
            videoPlane.materials = [playMaterial]
            
            let videoNode = SCNNode(geometry: videoPlane)
            videoNode.eulerAngles.x = -.pi / 2
            return videoNode
        }
        return nil
    }
}

