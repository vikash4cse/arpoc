//
//  AppDelegate.swift
//  NedbankAR
//
//  Created by Rupender on 04/07/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import UIKit
import ARKit
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        guard ARImageTrackingConfiguration.isSupported,
            ARWorldTrackingConfiguration.isSupported else {
                fatalError("ARKit is not available on this device.")
        }
        
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient,
                                                         mode: AVAudioSession.Mode.moviePlayback,
                                                         options: [.mixWithOthers])
        
        return true
    }
}
